ARG FLANNEL_VERSION="v0.11.0"
ARG VCS_URL
ARG VCS_REF

#
# Golang builder
#

FROM golang:alpine as builder

ARG FLANNEL_VERSION

WORKDIR /go/src/github.com/coreos/flannel

RUN apk add --update git alpine-sdk linux-headers

RUN git clone -q https://github.com/coreos/flannel.git -b "${FLANNEL_VERSION}" . \
    && go build -ldflags '-s -w -X github.com/coreos/flannel/version.Version=${FLANNEL_VERSION} -extldflags "-static"' -o /go/bin/flanneld

#
# Actual image
#

FROM alpine

ARG FLANNEL_VERSION
ARG VCS_URL
ARG VCS_REF

LABEL maintainer="Johan Fleury <jfleury@arcaik.net>"

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="flannel"
LABEL org.label-schema.description="Network fabric for containers, designed for Kubernetes"
LABEL org.label-schema.url="https://github.com/coreos/flannel"
LABEL org.label-schema.version="${FLANNEL_VERSION}"
LABEL org.label-schema.vcs-url="${VCS_URL}"
LABEL org.label-schema.vcs-ref="${VCS_REF}"

RUN apk add --update \
      ca-certificates \
      dumb-init \
      ip6tables \
      iproute2 \
      iptables \
      net-tools \
    && ln -sf xtables-nft-multi /sbin/ip6tables \
    && ln -sf xtables-nft-multi /sbin/ip6tables-restore \
    && ln -sf xtables-nft-multi /sbin/ip6tables-save \
    && ln -sf xtables-nft-multi /sbin/iptables \
    && ln -sf xtables-nft-multi /sbin/iptables-restore \
    && ln -sf xtables-nft-multi /sbin/iptables-save

COPY --from=builder /go/bin/flanneld /usr/local/bin

ENTRYPOINT ["dumb-init", "--"]
CMD ["flanneld"]
